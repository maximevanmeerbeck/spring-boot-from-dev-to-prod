package xdev.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import xdev.model.User;

public interface UserRepository extends MongoRepository<User, String> {

}